## amd64 image with browser-in-browser to connect to old IPMI KVMs
Based on: solarkennedy/ipmi-kvm-docker   

What you get (**bold marks state changes from original image**):  
Ubuntu 14.04 (**amd64**) as base,  
Xvfb - X virtual framebuffer,  
x11vnc - VNC server,  
Fluxbox - window manager,  
**Firefox 66.0.3**  - for browsing IPMI consoles,  
noVNC **1.3.0** - HTML5 canvas VNC viewer,  
icedtea-7-plugin (openJDK-7-jre included) - javaws supplier.  

### Best way to use:  
    docker run --rm -p 8080:8080 -d -v /mnt/:/mnt/ zerkalo7/ipmi   
Then go to browser and connect to http://localhost:8080/

Be aware that browser plugin not working. You should download and open jnlp file.

By default, the VNC session will run with a resolution of 1024x768 (with 24-bit color depth). Custom resolutions can be specified with the docker environment variable RES, and must include color depth.
```  
-e RES=1600x900x24
```

### noVNC clipboard transfer instructions:   
A solution for clipboard transfer is available. Text copied in the remote desktop will appear in the clipboard box in noVNC's interface. You can then copy the text from that box to access it in your local clipboard. Any text put into the clipboard box will be sent to the remote clipboard as well.  
https://novnc.com/img/noVNC-5-clipboard.png
